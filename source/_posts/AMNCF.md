﻿﻿---
title: Knowledge Based System
date: 2019-09-18
tags: Paper
---

# Attentive Multimodal Neural Collaborative Filtering for Group Recommendation

## 1. Abstract

Recent deep learning based group recommendation approaches have shown promising results and attracted a lot of concerns. However, most of them simply learn items' features only by employing their interactions and ratings data, which is not enough to make comprehensive representations of items. Moreover, studies using multimodal information for group recommendation are rarely explored. Thus, in order to make full use of items' multimodal information for better group recommendation results, we propose a novel model, namely AMNCF (short for “Attentive Multimodal Neural Collaborative Filtering for Group Recommendation"). Specifically, first we apply Hierarchical Attention Network and Residual Attention Network (intra-modal attention networks) to extract item's important visual and textual features, respectively. Then we concatenate them with the item's id embedding to generate the original multimodal embedding of item. Next, self-attention network (inter-modal attention network) is implemented to automatically assign weight to each modality, and then multiply each modality with its corresponding weight to form the final multimodal embedding of item. Afterwards, we also design attention network to learn the importance of group members. Finally, we jointly train group recommendation and personalized recommendation tasks on the same Neural Collaborative Filtering framework to reinforce each other. Experiments on two real world datasets show that our proposed method outperforms the baseline methods. Specifically, the hit rates of our proposed model on both datasets are improved by about 10% in both group recommendation and personalized recommendation tasks. Our work will also provide insights for utilizing multimodal information and attention mechanism to realize better group recommendation.

## 2. Datasets

We constructed the datasets by crawling data from IMDb and BookCrossing. (1) IMDb is a movie website where users can not only watch movies which they are interested in, but also they can make comments or rating for these films they watched. (2) BookCrossing is a book website where users can not only buy books which they like, but also they can make comments or rating for these books they bought.

- **Table 1 makes a simple statistics on the two datasets.**

|     Dataset     |  IMDb   | BookCrossing |
| :-------------: | :-----: | :----------: |
|   Total Users   |   610   |    19,480    |
|  Total Groups   |   239   |    2,579     |
|   Total Items   |  9,960  |    25,775    |
|  Total Ratings  | 100,734 |    9,5957    |
| Avg. Group Size |  2.55   |     7.55     |

## 3. Downloads

- **Code and Datasets**：<https://github.com/zscdumin/AMNCF>
